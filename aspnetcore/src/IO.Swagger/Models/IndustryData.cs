/*
 * Pace Gateway APIs
 *
 * Pace Software is a financial technology company that provides payment solutions through public web APIs.  The Pace Software solution includes a virtual terminal accessible through a web browser, a mobile application for Android, and public APIs for integration with other systems. The back-end of the solution is based on .NET framework, and the solution uses microservices architecture that provides for high scalability and easy maintenance.  Our public APIs enable merchants and third-party developers to integrate our Transactions solution into their systems. The APIs use RESTful architecture and JSON data format. The APIs are also PCI-compliant and use the latest security protocols to ensure the security of sensitive customer information.  The solution uses various security measures to ensure the security of sensitive customer information. The public APIs are all PCI-compliant, and they use the latest security protocols, including [AES-256](https://en.wikipedia.org/wiki/Advanced_Encryption_Standard) for packet-level encryption and decryption of API message and card tokenization to protect sensitive customer information.
 *
 * OpenAPI spec version: 2.7.0
 * Contact: support@pacesoft.net
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 */
using System;
using System.Linq;
using System.IO;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using Newtonsoft.Json;

namespace IO.Swagger.Models
{ 
    /// <summary>
    /// Contains all the sub-elements or fields that identify an industry and include data unique to that industry.
    /// </summary>
    [DataContract]
    public partial class IndustryData : IEquatable<IndustryData>
    { 
        /// <summary>
        /// Gets or Sets Industry
        /// </summary>
        [JsonConverter(typeof(Newtonsoft.Json.Converters.StringEnumConverter))]
        public enum IndustryEnum
        {
            /// <summary>
            /// Enum CardNotPresentEnum for card-not-present
            /// </summary>
            [EnumMember(Value = "card-not-present")]
            CardNotPresentEnum = 0,
            /// <summary>
            /// Enum FuelEnum for fuel
            /// </summary>
            [EnumMember(Value = "fuel")]
            FuelEnum = 1,
            /// <summary>
            /// Enum HotelEnum for hotel
            /// </summary>
            [EnumMember(Value = "hotel")]
            HotelEnum = 2,
            /// <summary>
            /// Enum RestaurantEnum for restaurant
            /// </summary>
            [EnumMember(Value = "restaurant")]
            RestaurantEnum = 3,
            /// <summary>
            /// Enum RetailEnum for retail
            /// </summary>
            [EnumMember(Value = "retail")]
            RetailEnum = 4,
            /// <summary>
            /// Enum SupermarketEnum for supermarket
            /// </summary>
            [EnumMember(Value = "supermarket")]
            SupermarketEnum = 5,
            /// <summary>
            /// Enum UndefinedEnum for undefined
            /// </summary>
            [EnumMember(Value = "undefined")]
            UndefinedEnum = 6        }

        /// <summary>
        /// Gets or Sets Industry
        /// </summary>
        [Required]

        [DataMember(Name="industry")]
        public IndustryEnum? Industry { get; set; }

        /// <summary>
        /// Gets or Sets Eci
        /// </summary>

        [DataMember(Name="eci")]
        public OneOfIndustryDataEci Eci { get; set; }

        /// <summary>
        /// Gets or Sets PurchaseId
        /// </summary>

        [MaxLength(25)]
        [DataMember(Name="purchaseId")]
        public string PurchaseId { get; set; }

        /// <summary>
        /// Gets or Sets MarketSpecificId
        /// </summary>
        [JsonConverter(typeof(Newtonsoft.Json.Converters.StringEnumConverter))]
        public enum MarketSpecificIdEnum
        {
            /// <summary>
            /// Enum AEnum for a
            /// </summary>
            [EnumMember(Value = "a")]
            AEnum = 0,
            /// <summary>
            /// Enum BEnum for b
            /// </summary>
            [EnumMember(Value = "b")]
            BEnum = 1,
            /// <summary>
            /// Enum HEnum for h
            /// </summary>
            [EnumMember(Value = "h")]
            HEnum = 2        }

        /// <summary>
        /// Gets or Sets MarketSpecificId
        /// </summary>

        [DataMember(Name="market_specific_id")]
        public MarketSpecificIdEnum? MarketSpecificId { get; set; }

        /// <summary>
        /// Gets or Sets PartialApproval
        /// </summary>

        [DataMember(Name="partialApproval")]
        public string PartialApproval { get; set; }

        /// <summary>
        /// Gets or Sets StoreAndForward
        /// </summary>

        [DataMember(Name="storeAndForward")]
        public string StoreAndForward { get; set; }

        /// <summary>
        /// Returns the string presentation of the object
        /// </summary>
        /// <returns>String presentation of the object</returns>
        public override string ToString()
        {
            var sb = new StringBuilder();
            sb.Append("class IndustryData {\n");
            sb.Append("  Industry: ").Append(Industry).Append("\n");
            sb.Append("  Eci: ").Append(Eci).Append("\n");
            sb.Append("  PurchaseId: ").Append(PurchaseId).Append("\n");
            sb.Append("  MarketSpecificId: ").Append(MarketSpecificId).Append("\n");
            sb.Append("  PartialApproval: ").Append(PartialApproval).Append("\n");
            sb.Append("  StoreAndForward: ").Append(StoreAndForward).Append("\n");
            sb.Append("}\n");
            return sb.ToString();
        }

        /// <summary>
        /// Returns the JSON string presentation of the object
        /// </summary>
        /// <returns>JSON string presentation of the object</returns>
        public string ToJson()
        {
            return JsonConvert.SerializeObject(this, Formatting.Indented);
        }

        /// <summary>
        /// Returns true if objects are equal
        /// </summary>
        /// <param name="obj">Object to be compared</param>
        /// <returns>Boolean</returns>
        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            return obj.GetType() == GetType() && Equals((IndustryData)obj);
        }

        /// <summary>
        /// Returns true if IndustryData instances are equal
        /// </summary>
        /// <param name="other">Instance of IndustryData to be compared</param>
        /// <returns>Boolean</returns>
        public bool Equals(IndustryData other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;

            return 
                (
                    Industry == other.Industry ||
                    Industry != null &&
                    Industry.Equals(other.Industry)
                ) && 
                (
                    Eci == other.Eci ||
                    Eci != null &&
                    Eci.Equals(other.Eci)
                ) && 
                (
                    PurchaseId == other.PurchaseId ||
                    PurchaseId != null &&
                    PurchaseId.Equals(other.PurchaseId)
                ) && 
                (
                    MarketSpecificId == other.MarketSpecificId ||
                    MarketSpecificId != null &&
                    MarketSpecificId.Equals(other.MarketSpecificId)
                ) && 
                (
                    PartialApproval == other.PartialApproval ||
                    PartialApproval != null &&
                    PartialApproval.Equals(other.PartialApproval)
                ) && 
                (
                    StoreAndForward == other.StoreAndForward ||
                    StoreAndForward != null &&
                    StoreAndForward.Equals(other.StoreAndForward)
                );
        }

        /// <summary>
        /// Gets the hash code
        /// </summary>
        /// <returns>Hash code</returns>
        public override int GetHashCode()
        {
            unchecked // Overflow is fine, just wrap
            {
                var hashCode = 41;
                // Suitable nullity checks etc, of course :)
                    if (Industry != null)
                    hashCode = hashCode * 59 + Industry.GetHashCode();
                    if (Eci != null)
                    hashCode = hashCode * 59 + Eci.GetHashCode();
                    if (PurchaseId != null)
                    hashCode = hashCode * 59 + PurchaseId.GetHashCode();
                    if (MarketSpecificId != null)
                    hashCode = hashCode * 59 + MarketSpecificId.GetHashCode();
                    if (PartialApproval != null)
                    hashCode = hashCode * 59 + PartialApproval.GetHashCode();
                    if (StoreAndForward != null)
                    hashCode = hashCode * 59 + StoreAndForward.GetHashCode();
                return hashCode;
            }
        }

        #region Operators
        #pragma warning disable 1591

        public static bool operator ==(IndustryData left, IndustryData right)
        {
            return Equals(left, right);
        }

        public static bool operator !=(IndustryData left, IndustryData right)
        {
            return !Equals(left, right);
        }

        #pragma warning restore 1591
        #endregion Operators
    }
}
