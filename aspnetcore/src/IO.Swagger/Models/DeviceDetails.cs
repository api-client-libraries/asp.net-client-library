/*
 * Pace Gateway APIs
 *
 * Pace Software is a financial technology company that provides payment solutions through public web APIs.  The Pace Software solution includes a virtual terminal accessible through a web browser, a mobile application for Android, and public APIs for integration with other systems. The back-end of the solution is based on .NET framework, and the solution uses microservices architecture that provides for high scalability and easy maintenance.  Our public APIs enable merchants and third-party developers to integrate our Transactions solution into their systems. The APIs use RESTful architecture and JSON data format. The APIs are also PCI-compliant and use the latest security protocols to ensure the security of sensitive customer information.  The solution uses various security measures to ensure the security of sensitive customer information. The public APIs are all PCI-compliant, and they use the latest security protocols, including [AES-256](https://en.wikipedia.org/wiki/Advanced_Encryption_Standard) for packet-level encryption and decryption of API message and card tokenization to protect sensitive customer information.
 *
 * OpenAPI spec version: 2.7.0
 * Contact: support@pacesoft.net
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 */
using System;
using System.Linq;
using System.IO;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using Newtonsoft.Json;

namespace IO.Swagger.Models
{ 
    /// <summary>
    /// 
    /// </summary>
    [DataContract]
    public partial class DeviceDetails : IEquatable<DeviceDetails>
    { 
        /// <summary>
        /// Name of the batch to which the device belongs
        /// </summary>
        /// <value>Name of the batch to which the device belongs</value>

        [DataMember(Name="batchName")]
        public string BatchName { get; set; }

        /// <summary>
        /// Gets or Sets Device
        /// </summary>

        [DataMember(Name="device")]
        public Device Device { get; set; }

        /// <summary>
        /// Gets or Sets DeviceGroup
        /// </summary>

        [DataMember(Name="deviceGroup")]
        public DeviceGroup DeviceGroup { get; set; }

        /// <summary>
        /// Gets or Sets UserDefined
        /// </summary>

        [DataMember(Name="userDefined")]
        public List<CustomPropertiesInput> UserDefined { get; set; }

        /// <summary>
        /// Sample response message for testing
        /// </summary>
        /// <value>Sample response message for testing</value>

        [DataMember(Name="echo")]
        public string Echo { get; set; }

        /// <summary>
        /// Returns the string presentation of the object
        /// </summary>
        /// <returns>String presentation of the object</returns>
        public override string ToString()
        {
            var sb = new StringBuilder();
            sb.Append("class DeviceDetails {\n");
            sb.Append("  BatchName: ").Append(BatchName).Append("\n");
            sb.Append("  Device: ").Append(Device).Append("\n");
            sb.Append("  DeviceGroup: ").Append(DeviceGroup).Append("\n");
            sb.Append("  UserDefined: ").Append(UserDefined).Append("\n");
            sb.Append("  Echo: ").Append(Echo).Append("\n");
            sb.Append("}\n");
            return sb.ToString();
        }

        /// <summary>
        /// Returns the JSON string presentation of the object
        /// </summary>
        /// <returns>JSON string presentation of the object</returns>
        public string ToJson()
        {
            return JsonConvert.SerializeObject(this, Formatting.Indented);
        }

        /// <summary>
        /// Returns true if objects are equal
        /// </summary>
        /// <param name="obj">Object to be compared</param>
        /// <returns>Boolean</returns>
        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            return obj.GetType() == GetType() && Equals((DeviceDetails)obj);
        }

        /// <summary>
        /// Returns true if DeviceDetails instances are equal
        /// </summary>
        /// <param name="other">Instance of DeviceDetails to be compared</param>
        /// <returns>Boolean</returns>
        public bool Equals(DeviceDetails other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;

            return 
                (
                    BatchName == other.BatchName ||
                    BatchName != null &&
                    BatchName.Equals(other.BatchName)
                ) && 
                (
                    Device == other.Device ||
                    Device != null &&
                    Device.Equals(other.Device)
                ) && 
                (
                    DeviceGroup == other.DeviceGroup ||
                    DeviceGroup != null &&
                    DeviceGroup.Equals(other.DeviceGroup)
                ) && 
                (
                    UserDefined == other.UserDefined ||
                    UserDefined != null &&
                    UserDefined.SequenceEqual(other.UserDefined)
                ) && 
                (
                    Echo == other.Echo ||
                    Echo != null &&
                    Echo.Equals(other.Echo)
                );
        }

        /// <summary>
        /// Gets the hash code
        /// </summary>
        /// <returns>Hash code</returns>
        public override int GetHashCode()
        {
            unchecked // Overflow is fine, just wrap
            {
                var hashCode = 41;
                // Suitable nullity checks etc, of course :)
                    if (BatchName != null)
                    hashCode = hashCode * 59 + BatchName.GetHashCode();
                    if (Device != null)
                    hashCode = hashCode * 59 + Device.GetHashCode();
                    if (DeviceGroup != null)
                    hashCode = hashCode * 59 + DeviceGroup.GetHashCode();
                    if (UserDefined != null)
                    hashCode = hashCode * 59 + UserDefined.GetHashCode();
                    if (Echo != null)
                    hashCode = hashCode * 59 + Echo.GetHashCode();
                return hashCode;
            }
        }

        #region Operators
        #pragma warning disable 1591

        public static bool operator ==(DeviceDetails left, DeviceDetails right)
        {
            return Equals(left, right);
        }

        public static bool operator !=(DeviceDetails left, DeviceDetails right)
        {
            return !Equals(left, right);
        }

        #pragma warning restore 1591
        #endregion Operators
    }
}
