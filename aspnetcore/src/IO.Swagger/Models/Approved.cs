/*
 * Pace Gateway APIs
 *
 * Pace Software is a financial technology company that provides payment solutions through public web APIs.  The Pace Software solution includes a virtual terminal accessible through a web browser, a mobile application for Android, and public APIs for integration with other systems. The back-end of the solution is based on .NET framework, and the solution uses microservices architecture that provides for high scalability and easy maintenance.  Our public APIs enable merchants and third-party developers to integrate our Transactions solution into their systems. The APIs use RESTful architecture and JSON data format. The APIs are also PCI-compliant and use the latest security protocols to ensure the security of sensitive customer information.  The solution uses various security measures to ensure the security of sensitive customer information. The public APIs are all PCI-compliant, and they use the latest security protocols, including [AES-256](https://en.wikipedia.org/wiki/Advanced_Encryption_Standard) for packet-level encryption and decryption of API message and card tokenization to protect sensitive customer information.
 *
 * OpenAPI spec version: 2.7.0
 * Contact: support@pacesoft.net
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 */
using System;
using System.Linq;
using System.IO;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using Newtonsoft.Json;

namespace IO.Swagger.Models
{ 
        /// <summary>
        /// Descriptor showing if the requested amount was approved, partially approved, or not approved.
        /// </summary>
        /// <value>Descriptor showing if the requested amount was approved, partially approved, or not approved.</value>
        [JsonConverter(typeof(Newtonsoft.Json.Converters.StringEnumConverter))]
        public enum Approved
        {
            /// <summary>
            /// Enum NotApprovedEnum for Not Approved
            /// </summary>
            [EnumMember(Value = "Not Approved")]
            NotApprovedEnum = 0,
            /// <summary>
            /// Enum PartialApprovalEnum for Partial Approval
            /// </summary>
            [EnumMember(Value = "Partial Approval")]
            PartialApprovalEnum = 1,
            /// <summary>
            /// Enum ApprovedEnum for Approved
            /// </summary>
            [EnumMember(Value = "Approved")]
            ApprovedEnum = 2        }
}
