/*
 * Pace Gateway APIs
 *
 * Pace Software is a financial technology company that provides payment solutions through public web APIs.  The Pace Software solution includes a virtual terminal accessible through a web browser, a mobile application for Android, and public APIs for integration with other systems. The back-end of the solution is based on .NET framework, and the solution uses microservices architecture that provides for high scalability and easy maintenance.  Our public APIs enable merchants and third-party developers to integrate our Transactions solution into their systems. The APIs use RESTful architecture and JSON data format. The APIs are also PCI-compliant and use the latest security protocols to ensure the security of sensitive customer information.  The solution uses various security measures to ensure the security of sensitive customer information. The public APIs are all PCI-compliant, and they use the latest security protocols, including [AES-256](https://en.wikipedia.org/wiki/Advanced_Encryption_Standard) for packet-level encryption and decryption of API message and card tokenization to protect sensitive customer information.
 *
 * OpenAPI spec version: 2.7.0
 * Contact: support@pacesoft.net
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 */
using System;
using System.Linq;
using System.IO;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using Newtonsoft.Json;

namespace IO.Swagger.Models
{ 
    /// <summary>
    /// 
    /// </summary>
    [DataContract]
    public partial class DeviceGroup : IEquatable<DeviceGroup>
    { 
        /// <summary>
        /// Unique identifier for the device group
        /// </summary>
        /// <value>Unique identifier for the device group</value>
        [Required]

        [DataMember(Name="id")]
        public string Id { get; set; }

        /// <summary>
        /// Name of the device group
        /// </summary>
        /// <value>Name of the device group</value>
        [Required]

        [DataMember(Name="name")]
        public string Name { get; set; }

        /// <summary>
        /// Total number of devices in the group
        /// </summary>
        /// <value>Total number of devices in the group</value>
        [Required]

        [DataMember(Name="deviceCount")]
        public int? DeviceCount { get; set; }

        /// <summary>
        /// Current status of the device group
        /// </summary>
        /// <value>Current status of the device group</value>
        [Required]

        [DataMember(Name="status")]
        public string Status { get; set; }

        /// <summary>
        /// Identifier of the client to which the device group belongs
        /// </summary>
        /// <value>Identifier of the client to which the device group belongs</value>
        [Required]

        [DataMember(Name="clientId")]
        public string ClientId { get; set; }

        /// <summary>
        /// Industry type of the device group
        /// </summary>
        /// <value>Industry type of the device group</value>

        [DataMember(Name="industry")]
        public string Industry { get; set; }

        /// <summary>
        /// POS mode used by the device group
        /// </summary>
        /// <value>POS mode used by the device group</value>

        [DataMember(Name="posMode")]
        public string PosMode { get; set; }

        /// <summary>
        /// Whether the device group has zip code prompts active.
        /// </summary>
        /// <value>Whether the device group has zip code prompts active.</value>

        [DataMember(Name="hasZipCodePrompt")]
        public bool? HasZipCodePrompt { get; set; }

        /// <summary>
        /// Whether the device group has CVV prompts active
        /// </summary>
        /// <value>Whether the device group has CVV prompts active</value>

        [DataMember(Name="hasCvvPrompt")]
        public bool? HasCvvPrompt { get; set; }

        /// <summary>
        /// Whether devices in the device group produce a receipt.
        /// </summary>
        /// <value>Whether devices in the device group produce a receipt.</value>

        [DataMember(Name="hasTipLine")]
        public bool? HasTipLine { get; set; }

        /// <summary>
        /// Whether receipts from devices in the device group recommend tip or gratuity amounts.
        /// </summary>
        /// <value>Whether receipts from devices in the device group recommend tip or gratuity amounts.</value>

        [DataMember(Name="hasTipRecommendation")]
        public bool? HasTipRecommendation { get; set; }

        /// <summary>
        /// Returns the string presentation of the object
        /// </summary>
        /// <returns>String presentation of the object</returns>
        public override string ToString()
        {
            var sb = new StringBuilder();
            sb.Append("class DeviceGroup {\n");
            sb.Append("  Id: ").Append(Id).Append("\n");
            sb.Append("  Name: ").Append(Name).Append("\n");
            sb.Append("  DeviceCount: ").Append(DeviceCount).Append("\n");
            sb.Append("  Status: ").Append(Status).Append("\n");
            sb.Append("  ClientId: ").Append(ClientId).Append("\n");
            sb.Append("  Industry: ").Append(Industry).Append("\n");
            sb.Append("  PosMode: ").Append(PosMode).Append("\n");
            sb.Append("  HasZipCodePrompt: ").Append(HasZipCodePrompt).Append("\n");
            sb.Append("  HasCvvPrompt: ").Append(HasCvvPrompt).Append("\n");
            sb.Append("  HasTipLine: ").Append(HasTipLine).Append("\n");
            sb.Append("  HasTipRecommendation: ").Append(HasTipRecommendation).Append("\n");
            sb.Append("}\n");
            return sb.ToString();
        }

        /// <summary>
        /// Returns the JSON string presentation of the object
        /// </summary>
        /// <returns>JSON string presentation of the object</returns>
        public string ToJson()
        {
            return JsonConvert.SerializeObject(this, Formatting.Indented);
        }

        /// <summary>
        /// Returns true if objects are equal
        /// </summary>
        /// <param name="obj">Object to be compared</param>
        /// <returns>Boolean</returns>
        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            return obj.GetType() == GetType() && Equals((DeviceGroup)obj);
        }

        /// <summary>
        /// Returns true if DeviceGroup instances are equal
        /// </summary>
        /// <param name="other">Instance of DeviceGroup to be compared</param>
        /// <returns>Boolean</returns>
        public bool Equals(DeviceGroup other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;

            return 
                (
                    Id == other.Id ||
                    Id != null &&
                    Id.Equals(other.Id)
                ) && 
                (
                    Name == other.Name ||
                    Name != null &&
                    Name.Equals(other.Name)
                ) && 
                (
                    DeviceCount == other.DeviceCount ||
                    DeviceCount != null &&
                    DeviceCount.Equals(other.DeviceCount)
                ) && 
                (
                    Status == other.Status ||
                    Status != null &&
                    Status.Equals(other.Status)
                ) && 
                (
                    ClientId == other.ClientId ||
                    ClientId != null &&
                    ClientId.Equals(other.ClientId)
                ) && 
                (
                    Industry == other.Industry ||
                    Industry != null &&
                    Industry.Equals(other.Industry)
                ) && 
                (
                    PosMode == other.PosMode ||
                    PosMode != null &&
                    PosMode.Equals(other.PosMode)
                ) && 
                (
                    HasZipCodePrompt == other.HasZipCodePrompt ||
                    HasZipCodePrompt != null &&
                    HasZipCodePrompt.Equals(other.HasZipCodePrompt)
                ) && 
                (
                    HasCvvPrompt == other.HasCvvPrompt ||
                    HasCvvPrompt != null &&
                    HasCvvPrompt.Equals(other.HasCvvPrompt)
                ) && 
                (
                    HasTipLine == other.HasTipLine ||
                    HasTipLine != null &&
                    HasTipLine.Equals(other.HasTipLine)
                ) && 
                (
                    HasTipRecommendation == other.HasTipRecommendation ||
                    HasTipRecommendation != null &&
                    HasTipRecommendation.Equals(other.HasTipRecommendation)
                );
        }

        /// <summary>
        /// Gets the hash code
        /// </summary>
        /// <returns>Hash code</returns>
        public override int GetHashCode()
        {
            unchecked // Overflow is fine, just wrap
            {
                var hashCode = 41;
                // Suitable nullity checks etc, of course :)
                    if (Id != null)
                    hashCode = hashCode * 59 + Id.GetHashCode();
                    if (Name != null)
                    hashCode = hashCode * 59 + Name.GetHashCode();
                    if (DeviceCount != null)
                    hashCode = hashCode * 59 + DeviceCount.GetHashCode();
                    if (Status != null)
                    hashCode = hashCode * 59 + Status.GetHashCode();
                    if (ClientId != null)
                    hashCode = hashCode * 59 + ClientId.GetHashCode();
                    if (Industry != null)
                    hashCode = hashCode * 59 + Industry.GetHashCode();
                    if (PosMode != null)
                    hashCode = hashCode * 59 + PosMode.GetHashCode();
                    if (HasZipCodePrompt != null)
                    hashCode = hashCode * 59 + HasZipCodePrompt.GetHashCode();
                    if (HasCvvPrompt != null)
                    hashCode = hashCode * 59 + HasCvvPrompt.GetHashCode();
                    if (HasTipLine != null)
                    hashCode = hashCode * 59 + HasTipLine.GetHashCode();
                    if (HasTipRecommendation != null)
                    hashCode = hashCode * 59 + HasTipRecommendation.GetHashCode();
                return hashCode;
            }
        }

        #region Operators
        #pragma warning disable 1591

        public static bool operator ==(DeviceGroup left, DeviceGroup right)
        {
            return Equals(left, right);
        }

        public static bool operator !=(DeviceGroup left, DeviceGroup right)
        {
            return !Equals(left, right);
        }

        #pragma warning restore 1591
        #endregion Operators
    }
}
