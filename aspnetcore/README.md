# IO.Swagger - ASP.NET Core 2.0 Server

Pace Software is a financial technology company that provides payment solutions through public web APIs.  The Pace Software solution includes a virtual terminal accessible through a web browser, a mobile application for Android, and public APIs for integration with other systems. The back-end of the solution is based on .NET framework, and the solution uses microservices architecture that provides for high scalability and easy maintenance.  Our public APIs enable merchants and third-party developers to integrate our Transactions solution into their systems. The APIs use RESTful architecture and JSON data format. The APIs are also PCI-compliant and use the latest security protocols to ensure the security of sensitive customer information.  The solution uses various security measures to ensure the security of sensitive customer information. The public APIs are all PCI-compliant, and they use the latest security protocols, including [AES-256](https://en.wikipedia.org/wiki/Advanced_Encryption_Standard) for packet-level encryption and decryption of API message and card tokenization to protect sensitive customer information.

## Run

Linux/OS X:

```
sh build.sh
```

Windows:

```
build.bat
```

## Run in Docker

```
cd src/IO.Swagger
docker build -t io.swagger .
docker run -p 5000:5000 io.swagger
```
